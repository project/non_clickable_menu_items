<?php

/**
 * @file non_clickable_menu_items.admin.inc
 * @see non_clickable_menu_items.module
 */

function non_clickable_menu_items_admin() {
  $value = variable_get('non_clickable_menu_items_context', '');
  $form['non_clickable_context'] = array(
    '#type' => 'textarea',
    '#title' => t('Parent elements CSS selectors'),
    '#default_value' => $value,
    '#description' => t('Provide one per line CSS selectors for HTML element which contain menu items. This could be useful when there are several links at page but you need to disable clicks for only one of them. In this case you need to specify parent elemnt CSS selector. Eg., ".nav-menu"'),
    '#rows' => count(explode("\r\n", $value)) + 1,
  );
  $value = variable_get('non_clickable_menu_items_urls', '');
  $form['non_clickable_urls'] = array(
    '#type' => 'textarea',
    '#title' => t('Non-Clickable URLs'),
    '#default_value' => $value,
    '#description' => t('Provide one URL per line. You could also edit menu items and set "Disable clicks" setting.'),
    '#rows' => count(explode("\r\n", $value)) + 1,
  );
  $form['action']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function non_clickable_menu_items_admin_submit($form, &$form_state) {
  // Check if system path has an alias and add it to the list.
  $urls = $form_state['values']['non_clickable_urls'];
  $list = array_map('trim', array_unique(array_filter(explode("\r\n", $urls))));
  foreach($list as $key => $item) {
    $alias = drupal_get_path_alias($item);
    if ($alias && strripos($urls, $alias) === FALSE) {
      // We are adding aliases to the end of list but they won't be
      // processed this time because PHP remembers only original $list.
      $list[] = $alias;
    }
  }
  variable_set('non_clickable_menu_items_urls', implode("\r\n", $list));
  variable_set('non_clickable_menu_items_context', $form_state['values']['non_clickable_context']);
}
